export default function Welcome(props){
	return(
		<>
			<h1>Hello, {props.name}, You are {props.height} tall.</h1>
			<p>Welcome to our Course Booking!</p>
		</>
	)
}