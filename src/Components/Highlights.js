import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
	return(
		<Row>
			<Col xs={12} md={4}>
				<Card className="cardHightlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>
								Learn From Home
							</h2>
						</Card.Title>
						<Card.Text>
							Lorem, ipsum dolor sit amet consectetur adipisicing, elit. Excepturi blanditiis cupiditate, adipisci quod laborum modi, nulla delectus debitis, iure id dolor sed mollitia expedita rerum alias asperiores, esse accusamus eveniet!
							Lorem ipsum dolor sit amet consectetur, adipisicing, elit. Id laboriosam laborum minus, ipsum est distinctio repudiandae pariatur eos ut eum neque velit accusantium iste expedita natus debitis voluptates provident consectetur.
						</Card.Text>	
					</Card.Body>	
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>
								Study Now, Pay Later
							</h2>
						</Card.Title>
						<Card.Text>
							Lorem, ipsum dolor sit amet consectetur adipisicing, elit. Excepturi blanditiis cupiditate, adipisci quod laborum modi, nulla delectus debitis, iure id dolor sed mollitia expedita rerum alias asperiores, esse accusamus eveniet!
							Lorem ipsum dolor sit amet consectetur, adipisicing, elit. Id laboriosam laborum minus, ipsum est distinctio repudiandae pariatur eos ut eum neque velit accusantium iste expedita natus debitis voluptates provident consectetur.
						</Card.Text>	
					</Card.Body>	
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>
								Sample
							</h2>
						</Card.Title>
						<Card.Text>
							Lorem, ipsum dolor sit amet consectetur adipisicing, elit. Excepturi blanditiis cupiditate, adipisci quod laborum modi, nulla delectus debitis, iure id dolor sed mollitia expedita rerum alias asperiores, esse accusamus eveniet!
							Lorem ipsum dolor sit amet consectetur, adipisicing, elit. Id laboriosam laborum minus, ipsum est distinctio repudiandae pariatur eos ut eum neque velit accusantium iste expedita natus debitis voluptates provident consectetur.
						</Card.Text>	
					</Card.Body>	
				</Card>
			</Col>
		</Row>
		)
}