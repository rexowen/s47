const coursesData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda fuga, voluptas dolor error deleniti debitis sunt explicabo cupiditate dolore est odio, ullam. At labore quae ipsam officiis ab omnis dolorem.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda fuga, voluptas dolor error deleniti debitis sunt explicabo cupiditate dolore est odio, ullam. At labore quae ipsam officiis ab omnis dolorem.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda fuga, voluptas dolor error deleniti debitis sunt explicabo cupiditate dolore est odio, ullam. At labore quae ipsam officiis ab omnis dolorem.",
		price: 55000,
		onOffer: true
	}

]
export default coursesData;